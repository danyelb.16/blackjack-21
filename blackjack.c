#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main() {
	
	char opjogo;								//Variaveis que armazenam as opcoes dos jogadores em relacao ao jogo.
	
	int jogada /*random*/, totalj = 0, totalc = 0; // caso o jogador vs computador	
	int total1 = 0, total2 = 0;		//caso jogador vs jogador				//Variaveis da jogada atual e do total de pontos.
	
	printf ("Bem Vindo ao BlackJack em C!!\n");
	printf ("Quantos jogadores participarao da partida?\n");
	printf ("Maximo dois(2) jogadores\n");
	
	scanf("%c",&opjogo); 
			
	if (opjogo == '1') 
	{
		//Vez do usuario.
		
		opjogo='S';
		system("cls");
		
		printf("Voce tem que fazer 21 pontos para ganhar, porem sem estourar esse limite, podendo parar apos cada jogada.\n");
		printf("\n");
		
		totalj=0;
		
		while ((opjogo=='s') || (opjogo=='S')) 	//LaÃ§o que deixa as jogadas a critÃ©rio do usuÃ¡rio.
		{
			printf("Deseja fazer a jogada?[S/N]\n");
			scanf(" %c", &opjogo);
			
			if ((opjogo=='n') || (opjogo=='N'))
				break;	
								
			srand(time(NULL));				//Gera numeros aleatorios de 1 ate 10.
			
			jogada=(rand() % 10)+1;
			
			totalj += jogada;
			
			printf("\n");
			printf("Voce tirou %d e ate agora marcou %d pontos.\n", jogada, totalj);
			printf("\n");
			
			if (totalj >= 21)
				break;
		}
			
		printf("Voce marcou %d pontos, vamos ver o computador...\n\n\n", totalj);
			
		//Vez do Computador
		printf("Agora e minha vez de jogar. Vejo que voce fez %d pontos...\n",totalj);
		totalc = 0;
		while (totalc < 21){
			
			srand(time(NULL));				//Gera nÃºmeros aleatÃ³rios de 1 atÃ© 10.
			jogada=(rand() % 10)+1;
			totalc=(totalc+jogada);
			
			if(totalc >= 21) 
				break;
				
			printf("Tirei %d pontos e pretendo continuar jogando, pois ainda estou com %d.\n",jogada,totalc);
			
			sleep(1);
		}
		
		printf("\n");
			
		printf("O jogador terminou com %d pontos e o computador com %d pontos, portanto...\n\n",totalj,totalc);
		if (totalj == 21) 
			printf("O jogador ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
			
		else if (totalc == 21) 
			printf("O computador ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
		
		else if (totalc == totalj)
			printf("Houve um empate...");
		
		else if (totalc > 21 && totalj <= 21) 
			printf("O computador tem um numero maior de pontos do que e permitido... O JOGADOR vence.\n"); 
			
		else if (totalj > 21 && totalc <= 21) 
			printf("O jogador tem um numero maior de pontos do que e permitido... O COMPUTADOR vence.\n"); 
			
		else if ((21 - totalc)>(21 - totalj)) 
			printf("O computador vence por estar mais perto de 21.\n"); 
		
		
		else
			printf("O jogador vence por estar mais perto de 21.\n");
		
			printf("\n");
	}
	
	else if (opjogo=='2') {
		
		printf("O jogo funciona por turnos, cada jogador tera sua vez e decidira o que fazer...\n");
		//Vez do jogador numero 1.
		opjogo='S';
		system("cls");
		
		printf("Voce tem que fazer 21 pontos para ganhar, porem sem estourar esse limite, podendo parar apos cada jogada.\n");
		printf("\n");
		totalj = 0;
		while ((opjogo=='s') || (opjogo=='S')) 	//Laco que deixa as jogadas a criterio do usuario.
		{
			printf("Deseja fazer a jogada?[S/N]\n");
			scanf(" %c",&opjogo);
			
			if ((opjogo=='n') || (opjogo=='N'))
				break;	
								
			srand(time(NULL));				//Gera numeros aleatorios de 1 ate 10.
			
			jogada=(rand() % 10)+1;
			
			total1=(total1+jogada);
			
			if (total1 >= 21)
				break;
				
			printf("\n");
			printf("Voce tirou %d e ate agora marcou %d pontos.\n", jogada, total1);
			printf("\n");
		}
		
		//Vez do jogador numero 2.
		opjogo='S';
		
		system("cls");
		
		printf("Voce tem que fazer 21 pontos para ganhar, porem sem estourar esse limite, podendo parar apos cada jogada.\n");
		
		printf("Cuidado, pois o jogador 1 marcou %d pontos", total1);
		printf("\n");
		
		totalj=0;
			while ((opjogo=='s') || (opjogo=='S')) 	//Laco que deixa as jogadas a criterio do usuario.
			{
				printf("Deseja fazer a jogada?[S/N]\n");
				scanf(" %c",&opjogo);
				if ((opjogo=='n') || (opjogo=='N'))
					break;	
					  				
				srand(time(NULL));				//Gera numeros aleatorios de 1 ate 10.
				jogada=(rand() % 10)+1;
				total2=(total2+jogada);
				
				if (total2 >= 21)
					break;
			
				printf("\n");
				printf("VocÃª tirou %d e atÃ© agora marcou %d pontos.\n",jogada,total2);
				printf("\n");
			}
			
			printf("\n");
			printf("O jogador 1 terminou com %d pontos e o jogador 2 com %d pontos, portanto...", total1, total1);
			
			if (total1 == 21) 
				printf("O jogador 1 ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
				
			else if (total2 == 21) 
				printf("O computador ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
				
			else if (total2 > 21 && total1 <= 21) 
				printf("O computador tem um nÃºmero maior de pontos do que Ã© permitido...O jogador vence.\n"); 
				
			else if (total1 > 21 && total2 <= 21) 
				printf("O jogador tem um nÃºmero maior de pontos do que Ã© permitido...O computador vence.\n"); 
				
			else if (total1 == total2)
				printf("Houve um empate..."); 
				
			else if ((21 - total2)>(21 - total1)) 
				printf("O jogador 2 vence por estar mais perto de 21.\n"); 
				
			else
				printf("O jogador 1 vence por estar mais perto de 21.\n");
				
				printf("\n"); 
			}
		else 
			printf("O maximo de jogadores permitidos e de 2\n");
}	 
  
  
  
  
  
